from django.shortcuts import render #, get_object_or_404, get_list_or_404
from todos.models import TodoList
# Create your views here.

def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "todolists": lists,
    }
    return render(request, "todos/list.html", context)
